## Simple NodeJS app + Slack

# Development
Run: npm run dev
OR
Docker Run: docker-compose up

# Production
Run: docker build --build-arg PORT=3555 -t vr-api:v1.0.0 .
docker run -p 3555:3555 -d vr-api:v1.0.0

# Explanation

This sample project illustrates the use of Api to send a message to specific slack channel. I have used postman app to test the API. 

Application is running at http://68.183.180.49:3555