import { 
    JsonController, 
    Post, 
    Body, 
    OnUndefined
} from 'routing-controllers';
import { IQuery } from '../interfaces/IQuery';
import { SendtoSlack } from '../models/SendMessage';

@JsonController()
export class PostMessageController {
    
    @Post('/send-to-slack')
    @OnUndefined(404)
    async sendAll(@Body() query: IQuery): Promise<Object> {
        try {
            let send = await SendtoSlack.sendMessage(query);
            return send;
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }
}