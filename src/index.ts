import {createExpressServer, useContainer } from "routing-controllers";
import { Controllers } from './controllers';
import 'reflect-metadata';

var app = createExpressServer({
        controllers: Controllers,
        cors: {
            methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
            allowedHeaders: ["X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"],
            exposedHeaders: ['X-Total-Length', 'Authorization', 'Content-Type', 'X-Total-Page'],
            credentials: false   
        }
    })

const port = process.env.PORT || '3555';
    
app.listen(port, (err) => {
    if(err) {
        console.log(`unable to start server due to ${err}`);
    }
        console.log(`server running at ${port}`);
});

export default app; 