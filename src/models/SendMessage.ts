import { IQuery } from '../interfaces/IQuery';
import Slack = require("slack-node");

let webhookUri = "https://hooks.slack.com/services/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";
let slack = new Slack();
slack.setWebhook(webhookUri);

export class SendtoSlack {
    static async sendMessage(query: IQuery): Promise<Object> {
        try {
            const slackMessage = await "*Chat Support Request*\nFirstname: *"+query.firstName+
            "*\nLastname: *"+query.lastName+
            "*\nEmail: *"+query.email+
            "*\nMessage: *"+query.message+"*"

            slack.webhook({
                channel: "#chat-support",
                username: "Support BOT",
                icon_emoji: ":ghost:",
                text: slackMessage
              }, function(err, response) {});

            return `Thank you ${query.firstName} ${query.lastName}. We will contact you soon at ${query.email}`;        
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }
}