import * as chai from 'chai';
import chaiHttp = require('chai-http');
import 'mocha';
import "reflect-metadata";
const app = require('../../src').default;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
process.env.NODE_ENV = 'test';
chai.use(chaiHttp);
const expect = chai.expect;
import { IQuery } from '../interfaces/IQuery';

const userData: IQuery = {
    firstName: 'gowtham',
    lastName: 'test',
    email: 'test@gowtham.com',
    message: 'how are you?'
}

describe('/send-to-slack', () => {
    it('Test: Should get status 200', async () => {
        const results = await chai.request(app).post('/send-to-slack').send(userData);
        expect(results).to.have.status(200)
        expect(results.body)
    })
})