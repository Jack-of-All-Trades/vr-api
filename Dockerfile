FROM keymetrics/pm2:10-alpine

ARG PORT

ENV PORT "$PORT"

COPY ./ /app

WORKDIR /app

RUN apk update && \
apk add curl ca-certificates && rm -rf /var/cache/apk/* && \ 
pm2 install pm2-auto-pull

RUN npm install && npm run prod

ENV NODE_ENV=production

EXPOSE ${PORT}

CMD ["pm2-docker", "start", "--json", ".build/main.js"]